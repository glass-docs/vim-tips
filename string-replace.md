## VIM Search and replace syntax

The syntax is as follows:  
`:%s/Search/Replace/CommandFlag`  

Common options include:
- This line only:  `:s/Search-Word/Replace-Word/g`  
- Every line: `:%s/Search-Word/Replace-Word/g`  
- Case insensitive: `:%s/Search-Word/Replace-Word/gi`  
- Ask for confirmation: `:%s/Search-Word/Replace-Word/gc`

Examples:
- Search and replace in the current line only
`:s/Find-Word/Replace-Word/g  `  

- Change each 'Unix' to 'Linux' for all lines from line 36 to line 42 
`:36,42s/Unix/Linux/g`

- Delete all instances of "<sysadmin>":  
`:%s/sysadmin\>//g`

- Delete every occurrence of the whole word “method_” and the following 3 characters:
`:%s/method_.\{3}//`
